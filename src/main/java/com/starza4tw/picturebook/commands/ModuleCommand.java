package com.starza4tw.picturebook.commands;

import com.starza4tw.picturebook.ConfigurationManager;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;

public class ModuleCommand implements CommandExecutor 
{
    @Override public CommandResult execute(CommandSource source, CommandContext args) throws CommandException
    {
        ConfigurationManager.editNode(new Object[] {"modules", args.<String>getOne("module").get()} , args.<Boolean>getOne("bool").get());
        ConfigurationManager.saveConfiguration();
        ConfigurationManager.loadConfiguration();
        source.sendMessage(Text.of("Module Updated."));
        return CommandResult.success();
    }
}